﻿using EXNCars.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EXNCars.Data.Context
{
    public class EXNCarsContext : DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            dbContextOptionsBuilder.UseSqlServer(@"Server=LAPTOP-26\FIIPRACTIC;Database=EXNCars;Trusted_Connection=True;");
        }

    }
}
