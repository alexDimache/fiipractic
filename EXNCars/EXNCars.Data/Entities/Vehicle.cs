﻿using System;

namespace EXNCars.Data.Entities
{
    public class Vehicle
    {
        public int Id { get; set; }
        public string LicensePlate { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Color { get; set; }
    }
}
