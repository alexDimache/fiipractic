﻿using EXNCars.Data.Context;
using EXNCars.Data.Entities;
using System;
using System.Linq;

namespace EXNCars
{
    class Program
    {
        static void Main(string[] args)
        {
            #region comentat
            ////var lista = new List<int>() { 1, 2, 3, 2 };
            ////var altaLista = new List<int>();

            ////altaLista.Add(1);
            ////altaLista.AddRange(lista);

            //////lista.Remove(2);
            //////lista.Remove(2);
            //////lista.RemoveRange(1, 3);
            ////lista.RemoveAll(x => x % 2 == 0);

            ////for (int i = 0; i < lista.Count; i++)
            ////{
            ////    Console.Write($"{lista[i]} ");
            ////}
            //var vehiculNegru = new Vehicle()
            //{
            //    Id = 3,
            //    Color = "black",
            //    Age = 6
            //};
            //var listaVehicule = new List<Vehicle>()
            //{
            //    new Vehicle()
            //    {
            //        Id=1,
            //        Color="red",
            //        Age=3
            //    },

            //    new Vehicle()
            //    {
            //        Id=1,
            //        Color="red",
            //        Age=3
            //    },

            //    new Vehicle()
            //    {
            //        Id=4,
            //        Color="purple",
            //        Age=4,
            //        Country = "Olanda"
            //    },

            //    vehiculNegru,
            //    vehiculNegru
            //};

            ////foreach (var vehicul in listaVehicule)
            ////{
            ////    Console.Write($"Id: {vehicul.Id} Color: {vehicul.Color} Age: {vehicul.Age}\n");
            ////}

            //var listaVehiculeRosii = listaVehicule.Where(x => x.Color == "red");
            //var listVehiculeRosiiDesc = listaVehiculeRosii.OrderByDescending(x => x.Age).FirstOrDefault();
            //var listaCulori = listaVehicule.Select(x => new { Culoare = x.Color, Tara = x.Country ?? "Romania" });
            //var vehicul = listaVehicule.Where(x => x.Color == "red")
            //    .Where(x => x.Age == 100)
            //    .FirstOrDefault();

            //foreach (var vehicul in listaCulori)
            //{
            //    Console.Write($"{vehicul.Culoare} {vehicul.Tara} ");
            //    Console.WriteLine();
            //}

            //foreach (var vehicul in listVehiculeRosiiDesc)
            //{
            //    Console.Write($"Id: {vehicul.Id} Color: {vehicul.Color} Age: {vehicul.Age}\n");
            //}
            #endregion

            var context = new EXNCarsContext();

            var vehiculRosu = context.Vehicles.Where(x => x.Color.Equals("red")).FirstOrDefault();

            //var vehiculNou = new Vehicle()
            //{
            //    Color = "red",
            //    LicensePlate = "IS01IAS",
            //    RegistrationDate = DateTime.Now
            //};
            //var vehiculNou2 = new Vehicle()
            //{
            //    Color = "blue",
            //    LicensePlate = "IS09SAI",
            //    RegistrationDate = DateTime.Now
            //};

            //context.Vehicles.Add(vehiculNou);
            //context.Vehicles.Add(vehiculNou2);

            vehiculRosu.Color = "purple";
            context.SaveChanges();
        }
    }

    //class Vehicle
    //{
    //    public int Id { get; set; }

    //    public int? Age { get; set; }

    //    public string Color { get; set; }

    //    public string Country { get; set; }
    //}
}
